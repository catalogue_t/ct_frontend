# BUILD PROJECT DISTR
FROM node:14.16.0-alpine3.13 AS build
WORKDIR /src
COPY ["package.json", "./"]
RUN npm install
COPY . .
RUN npm run build

# BUILD FINAIL IMAGE
FROM nginx:1.19.8-alpine
COPY --from=build /src/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]